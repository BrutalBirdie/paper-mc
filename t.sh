#!/bin/bash

get_vars () {
    read -p "Cloudron E-Mail: " EMAIL
    read -p "Cloudron Username: " USERNAME
    read -s -p "Cloudron Password: " PASSWORD
    read -p "Cloudron URL: " CLOUDRON_URL
    read -p "DOCKER REPOSITORY URL: " DOCKER_REPOSITORY_URL
    read -p "DOCKER USERNAME: " DOCKER_REPOSITORY_USERNAME
    read -p -s "DOCKER PASSWORD: " DOCKER_REPOSITORY_PASSWORD


    printf "EMAIL=$EMAIL\n
            USERNAME=$USERNAME\n
            PASSWORD=$PASSWORD\n
            DOCKER_REPOSITORY_URL=$DOCKER_REPOSITORY_URL\n
            DOCKER_REPOSITORY_USERNAME=$DOCKER_REPOSITORY_USERNAME\n
            DOCKER_REPOSITORY_PASSWORD=$DOCKER_REPOSITORY_PASSWORD\n
            " > .env
}

# check if .env file exists and read it or check if vars exsist
if [ -f ".env" ]; then
    export $(egrep -v '^#' .env | xargs) &> /dev/null
else
    echo ".env File missing - asking for required vars"
    get_vars
fi

echo "=> Running tests"
cd test
echo "=> Installing package.json"
npm install
if [[ -f ./node_modules/.bin/mocha ]]; then
    ./node_modules/.bin/mocha ./test.js -b
fi

if [[ -f ../node_modules/.bin/mocha ]]; then
    ../node_modules/.bin/mocha ./test.js -b
fi

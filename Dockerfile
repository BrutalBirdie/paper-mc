FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code
ENV MC_VERSION=1.18.2 \
    PMC_VERSION=376

RUN apt-get update && apt-get install -y openjdk-17-jdk-headless && rm -rf /var/cache/apt /var/lib/apt/lists

# https://www.minecraft.net/en-us/download/server/
RUN curl -L https://api.papermc.io/v2/projects/paper/versions/${MC_VERSION}/builds/${PMC_VERSION}/downloads/paper-${MC_VERSION}-${PMC_VERSION}.jar -o minecraft_server.jar

COPY frontend /app/code/frontend
COPY backend /app/code/backend
COPY index.js package.json package-lock.json start.sh /app/code/

RUN npm install && \
    chown -R cloudron:cloudron /app/code/backend /app/code/frontend

CMD [ "/app/code/start.sh" ]
